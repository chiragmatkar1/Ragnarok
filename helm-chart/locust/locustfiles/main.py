from locust import HttpUser, between, task

class WebsiteUser(HttpUser):
    wait_time = between(5, 15)

    @task
    def algolia_getall(self):
        self.client.get("http://tira.jiohostx2.de/ext/algolia/application/api/v1.0/products")

    @task(2)
    def algolia_query(self):
        self.client.get("https://tira.jiohostx2.de/ext/algolia/application/api/v1.0/auto-complete?q=perfume")
